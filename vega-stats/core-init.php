<?php 
/*
*
*	***** Vega Stats *****
*
*	This file initializes all VS Core components
*	
*/
// If this file is called directly, abort. //
if ( ! defined( 'WPINC' ) ) {die;} // end if
// Define Our Constants
define('VS_CORE_INC',dirname( __FILE__ ).'/assets/inc/');
define('VS_CORE_IMG',plugins_url( 'assets/img/', __FILE__ ));
define('VS_CORE_CSS',plugins_url( 'assets/css/', __FILE__ ));
define('VS_FA_CSS',plugins_url( 'assets/css/', __FILE__ ));
define('VS_FA_SOLID',plugins_url( 'assets/css/', __FILE__ ));
define('VS_FA_BRAND',plugins_url( 'assets/css/', __FILE__ ));
define('VS_JQUERY',plugins_url( 'assets/js/', __FILE__ ));
define('VS_CORE_JS',plugins_url( 'assets/js/', __FILE__ ));
/*
*
*  Register CSS
*
*/

function vs_register_core_css(){
wp_enqueue_style('vs-core', VS_CORE_CSS . 'vs-core.css',null,time('s'),'all');
wp_enqueue_style('vs-fa', VS_FA_CSS . 'fontawesome.min.css',null,time('s'),'all');
wp_enqueue_style('vs-fa-solid', VS_FA_SOLID . 'solid.min.css',null,time('s'),'all');
wp_enqueue_style('vs-fa-brand', VS_FA_BRAND . 'brands.min.css',null,time('s'),'all');
};
add_action( 'wp_enqueue_scripts', 'vs_register_core_css' );        
/*
*
*  Register JS/Jquery Ready
*
*/
function vs_register_core_js(){
// Register Core Plugin JS	
wp_enqueue_script('vs-core', VS_CORE_JS . 'vs-core.js','jquery',time(),true);
wp_enqueue_script('vs-jquery', VS_JQUERY . 'jquery.min.js','jquery',time(),true);

};
add_action( 'wp_enqueue_scripts', 'vs_register_core_js' );    

add_action('wp_head', 'plugin_ajaxurl');

function plugin_ajaxurl() {

   echo '<script type="text/javascript">
           var ajax_url = "' . admin_url('admin-ajax.php') . '";
         </script>';
}
/*
*
*  Includes
*
*/ 
// Load the Functions
if ( file_exists( VS_CORE_INC . 'vs-core-functions.php' ) ) {
	require_once VS_CORE_INC . 'vs-core-functions.php';
}     
// Load the ajax Request
if ( file_exists( VS_CORE_INC . 'vs-ajax-request.php' ) ) {
	require_once VS_CORE_INC . 'vs-ajax-request.php';
} 
// Load the Shortcodes
if ( file_exists( VS_CORE_INC . 'vs-shortcodes.php' ) ) {
	require_once VS_CORE_INC . 'vs-shortcodes.php';
}