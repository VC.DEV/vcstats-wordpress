jQuery(document).ready( function(){         
    $( ".vc-query" ).click(function() { 
		let query = $("#query").val()
		  $("#test").empty();
		var data = {
	           'action' : 'get_stats',
	           'query' : $("#query").val()
	       };
		   
		   $("#query").val("Seaching...");
		   $('#mySpinner').addClass('spinner');
		   $('#mySpinner').removeClass('fa-search');

		   jQuery.ajax({
		              url : ajax_url,
		              type : 'post',
		              data : data,
		              success : function(response) {
						 $("#query").val("");	
						 console.log(response);
				
						
						  var stats = JSON.parse(response);
  						  if (stats.status == 200){
  				   		   $('#mySpinner').addClass('fa-search');
  				   		   $('#mySpinner').removeClass('spinner');


  						  $("#test").append('<div class="result">'+
  							  '<div class="row"> <div class="col-md-4"><img class="img img-responsive" src="' + stats.info.avatar + '"></div>'+
  							  '<div class="col-md-8">' + 
						      '<div class="col-md-12"><p class="info">'+stats.info.name+'</p></div>'+
						      '<div class="row info_icn"><div class="col-md-3"><i class="fas fa-level-up-alt"></i>  '+stats.info.level+'</div><div class="col-md-4"><i class="fas fa-trophy"></i> '+stats.info.medals+'</div><div class="col-md-5"><i class="fas fa-globe-europe"></i>  '+stats.info.planet+'</p></div></div> ' +
						   
						   '</div><div class="col-md-12"><i class="fas fa-sort-down"></i></div></div>' +
  							  '</div>'
  						  );
					  } else {
				   		   $('#mySpinner').addClass('fa-search');
				   		   $('#mySpinner').removeClass('spinner');
					   $("#query").val("Player '"+query+"' not found");
					  }
					 },
						error : function(request, status, error){

						}
		              });
		         


	});
});