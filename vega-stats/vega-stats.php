<?php 
/*
Plugin Name: Vega Stats
Plugin URI: https://vegaconflict.co/plugins/vcstats
Description: Add Vega Conflict Stats to WordPress
Version: 0.01
Author: JAG
Author URI: https://vegaconflict.co
*/

require_once( plugin_dir_path( __FILE__ ) . 'core-init.php');

class VC_Stats_Widget extends WP_Widget {
	
	public function __construct() {
		
		if (!defined('WPINC')) {die;} 
		
		$widget_ops = array( 
			'classname' => 'vc_stats_widget',
			'description' => 'Displays player statistics from vegaconflict.co',
		);
		
		add_action( 'wp_ajax_nopriv_get_stats', array( $this, 'get_stats' ) );
		add_action( 'wp_ajax_get_stats', array( $this, 'get_stats' ) );
		
		
		parent::__construct( 'vc_stats_widget', 'VegaConflict Player Stats', $widget_ops );
	}
		
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		echo '<div id="custom-search-input">
                  <div class="input-group">
                       <input type="text" class="search-query form-control" id="query" placeholder="Find Player Stats" autocomplete="off"/>
                       <span class="input-group-btn" style="z-index: 2;width:0px">
                       <button class="vc-query" type="button">
                      		<span id="mySpinner" class="fas fa-search"></span>
                       </button>
                       </span>
								
                  </div>
             </div>
			 
			 <div class="test" id="test"></div>
			 <div class="vc-power">
			 <a href="https://vegaconflict.co" target="blank">Powered by vegaconflict.co</a>
			 </div>'
			 ;
	}
	

	
	function get_stats()
	{
		if (isset($_POST['query'])) {
				$response = wp_remote_get('https://api.vegaconflict.co/player/stats/'.$_POST['query']);
				$body = wp_remote_retrieve_body($response);
				echo $body;
				die();
				} else {
					echo "dead";
					die();
				}
	}
	
}



add_action( 'widgets_init', function(){
	register_widget( 'VC_Stats_Widget' );
});
